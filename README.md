# Animating handwritten characters #

Exercising html canvas animations

### What am I doing in this repo? ###

* Playing with bezier curves to simulate animated transitions between handwritten characters.
* I was inspired by http://jackf.net/bezier-clock/ but I haven't looked at its code.

### How do I get set up? ###

Just open the html file in a browser. Preferably Chrome.
